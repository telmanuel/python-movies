"""This is a module for movies"""

class Movie(object):
    """This is a movie"""
    def __init__(self, genre, length):
        self.genre = genre
        self.length = length
